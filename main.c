/*
Karolis Kraujelis

Užduoties terminas: 10.31
Užduoties tikslas: parodyti gerą mokėjimą dirbti su Unix procesais.
Užduoties reikalavimai:

turi būti parašyta C kalba. (CHECK)
turi turėti pakankamai komentarų. (CHECK)
turi veikti fakulteto linux kompiuteryje. (CHECK)
turi turėti veikiantį Makefile, ir kompiliuojama naudojanti jį. (CHECK)
programa turi rodyti pakvietimą. (CHECK)
turi priimti komandas, jas apdoroti. (CHECK?)
darbas baigiasi gavus EOF, arba paršius exit. (CHECK)
turi būti įgyvendinta vienas iš šių reikalavimų:
 * konvejeriai. (CHECK)
 * foninis apdorojimas.
 * srautų nukreipimas.
Kiti reikalavimai: stiliaus, programos skaitomomu, klaidų nebuvimo, kompiliavimo (-Wall) perspėjimų nebuvimas, programos rašomos su Vi/Emacs, ...
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>

#include <sys/wait.h>

/**
 * Defines increment length of input buffer.
 */
#define INPUT_BUFFER_LEN 1024

/**
 * Defines possible characters that can be used as pipe character.
 */
#define INPUT_PIPE_CHAR "|"

//Buffer is always null terminated string.
//Null terminated resizeable string with append function.
struct DynamicString {
    char *buffer;
    size_t buffLen;
};
typedef struct DynamicString dstring;

dstring *ds_create_empty() {
    dstring *res = malloc(sizeof(dstring));
    res->buffer = malloc(INPUT_BUFFER_LEN);
    res->buffer[0] = '\0';
    res->buffLen = 1;
    return res;
}

void ds_free(dstring *dynamicString) {
    free(dynamicString->buffer);
    free(dynamicString);
}

bool ds_empty(dstring const *dynamicString) {
    return dynamicString->buffLen == 1 && dynamicString->buffer[0] == '\0';
}

void ds_append(dstring *dynamicString, const char *text) {
    size_t textLen = strlen(text);
    size_t newLen = dynamicString->buffLen + textLen;

    char *newBuffer = realloc(dynamicString->buffer, newLen);
    if (newBuffer == NULL) {
        ds_free(dynamicString); //if realloc failed we need to deallocate memory.
        assert(false);
    }

    dynamicString->buffer = newBuffer;
    dynamicString->buffLen = newLen;

    strcat(dynamicString->buffer, text);
}

/**
 * Greets user after starting program.
 */
void greetUser() {
    printf("Hello to custom shell\n");
    printf("Written by: Karolis Kraujelis\n");
    printf("\n\nEnter any command to proceed:\n");
}

/**
 * Prints prompt before every user input.
 */
void printPrompt() {
    printf("*: ");
}

//#define DEBUG

/**
 * Prints debug message.
 *
 * @param format
 * @param ...
 */
void printDebug(const char *format, ...) {
#ifdef DEBUG
    va_list arglist;
    printf("[DEBUG]: ");
    va_start(arglist, format);
    vprintf(format, arglist);
    va_end(arglist);
    printf("\n");
#endif
}

/**
 * Reads user inputted text
 *
 * @returns DynamicString
 */
dstring *readUserInput() {
    char buffer[INPUT_BUFFER_LEN];
    dstring *result = NULL;

    printPrompt();
    while (fgets(buffer, INPUT_BUFFER_LEN, stdin) != NULL) {
        bool breakAfterCat = false;

        if (result == NULL)
            result = ds_create_empty();

        //Lets filter newline and read till newline.
        size_t bufferLen = strlen(buffer);
        size_t bufferBeforeNewline = strcspn(buffer, "\n");

        if (bufferBeforeNewline != bufferLen) {
            buffer[bufferBeforeNewline] = '\0'; //Lets omit everything past newline.
            breakAfterCat = true;
        }

        //And append what we got.
        ds_append(result, buffer);
        if (breakAfterCat)
            break;
    }

    return result;
}

/**
 * removes spaces from left to right.
 *
 * @param s
 * @return
 */
char *ltrim(char *s) {
    while (isspace(*s)) s++;
    return s;
}

/**
 * removes spaces from right to left.
 *
 * @param s
 * @return
 */
char *rtrim(char *s) {
    char *back = s + strlen(s);
    while (isspace(*(--back))) {
        *(back) = '\0';
    }

    return s;
}

/**
 * Counts spaces in a string. (If two spaces are connected they are counted as one)
 *
 * @param s
 * @return
 */
int countSpaces(char *s) {
    int result = 0;

    size_t slen = strlen(s);

    bool lastSpace = false;
    for (size_t i = 0; i < slen; i++) {
        if (isspace(s[i]) && !lastSpace) {
            lastSpace = true;
            result++;
        }

        if(!isspace(s[i]))
            lastSpace = false;
    }

    return result;
}

/**
 * Counts connected spaces from s.
 *
 * @param s
 * @return
 */
int countConnectedSpaces(char *s) {
    int result = 0;
    while(isspace(*s) && (*s) != '\0')
    {
        result++;
        s += 1;
    }

    return result;
}

/**
 * Runs command in child process
 *
 * @param command - command to run.
 * @return file descriptor of command output
 */
int runCommand(const char *command, int pipeIn, bool last) {
    printDebug("Parent process is creating output pipe!");

    bool hasInput = (pipeIn != -1);
    int pipeFd[2];

    if(!last) {
        assert(pipe(pipeFd) != -1);
    }
    int pid = fork();
    assert(pid >= 0);

    if (pid > 0) {
        //Parent
        if(!last) {
            assert(close(pipeFd[1]) == 0); //Closes writing part of output pipe.

            return pipeFd[0];
        } else {
            return -1;
        }
    } else {
        //Child.
        printDebug("Child process is running command... (%s)", command);

        //Parses command.
        size_t oldlen = strlen(command);
        size_t commandSize = (oldlen + 1) * sizeof(char);
        char *commandCopy = malloc(commandSize);
        assert(commandCopy != NULL);
        strncpy(commandCopy, command, commandSize);
        commandCopy = ltrim(commandCopy);
        commandCopy = rtrim(commandCopy);

        int spacesCount = countSpaces(commandCopy);
        char **args = malloc(sizeof(char *) * (spacesCount + 2));
        assert(args != NULL);


        args[0] = (char *) commandCopy;
        if (spacesCount == 0)
            printf("Calling command %s without args\n", commandCopy);

        for (int i = 0; i < spacesCount; i++) {
            size_t spaceAt = strcspn(args[i], " ");

            size_t skipChars = countConnectedSpaces(args[i] + spaceAt);
            for(size_t j = 0; j < skipChars; j++) {
                args[i][spaceAt + j] = '\0';
            }

            args[i + 1] = args[i] + spaceAt + skipChars;

            if(i == 0) {
                if (spacesCount != 0)
                    printf("Calling command %s with args (", commandCopy);
            } else {
                printf("%s, ", args[i]);
            }
        }

        if (spacesCount != 0)
            printf("%s)\n", args[spacesCount]);

        args[spacesCount + 1] = NULL;

        if (hasInput) { //if we have input specified we redirect in pipe to stdin.
            assert(dup2(pipeIn, STDIN_FILENO) != -1);
            assert(close(pipeIn) == 0);
        }

        int old_stdout = dup(STDOUT_FILENO);
        if(!last) {
            assert(close(pipeFd[0]) == 0); //Closes reading part of pipe.
            assert(dup2(pipeFd[1], STDOUT_FILENO) != -1); //Redirect stdout to pipe.
            assert(close(pipeFd[1]) == 0); //We don't need this anymore cuz duplicated.
        }

        execvp(args[0], args);

        //If we're here then it means we got error.
        if (errno == ENOENT) {

            if(!last) {
                dup2(old_stdout, STDOUT_FILENO);
            }
            printf("Command \"%s\" doesn't exist\n", args[0]);
        }


        exit(0);
    }
}

/**
 * Runs user input.
 *
 * @param input
 */
void runUserInput(dstring* input) {
    //We split entered command into tokens.
    char *token = strtok(input->buffer, INPUT_PIPE_CHAR);
    int currentPipe = -1;
    int tokenCount = 0;
    while (token != NULL) {
        printDebug("Found command: %s", token);

        char* tokenCopy = malloc((strlen(token) + 1) * sizeof(char));
        assert(tokenCopy != NULL);
        strcpy(tokenCopy, token);

        token = strtok(NULL, INPUT_PIPE_CHAR);

        tokenCount++;
        currentPipe = runCommand(tokenCopy, currentPipe, token == NULL);
        free(tokenCopy);
    }

    for(int i = 0; i < tokenCount; i++) {
        wait(NULL);
    }
}

int main(int argc, char **argv) {
    greetUser();

    while (true) {
        dstring *input = readUserInput();
        if (input == NULL)
            break;

        if (ds_empty(input)) {
            ds_free(input);
            continue;
        }

        if (strcmp(input->buffer, "exit") == 0) {
            ds_free(input);
            break;
        }

        printDebug("User entered: %s", input->buffer);
        runUserInput(input);

        ds_free(input);
    }

    printf("\n");

    return 0;
}